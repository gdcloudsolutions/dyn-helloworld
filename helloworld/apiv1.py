# Copyright (c) 2018 Groupe Dynamite.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""apiv1 module

Expose helloworld APIs
"""

from flask import Blueprint
from flask_restplus import Api

from .apis.health import api as ns_health
from .apis.hello import api as ns_hello
from .apis.breakit import api as ns_breakit

blueprint = Blueprint('apiv1', __name__)
api = Api(blueprint,
    title='Helloworld service',
    version='1.0',
    description='Groupe Dynamite helloworld Program',
    #doc=True
)

api.add_namespace(ns_health, path='/')
api.add_namespace(ns_hello)
api.add_namespace(ns_breakit)
