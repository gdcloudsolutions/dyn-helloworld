# Copyright (c) 2018 Groupe Dynamite.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Email"""

from flask import request
from flask_restplus import Namespace, Resource, fields

api = Namespace('hello', description='Hello')

model_hello = api.model('Hello', {
    'who': fields.String(required=True, description='Say hello to who?', example='World')
})

@api.route('/')
class Hello(Resource):
    @api.doc('Hello who?')
    @api.response(200, 'Hello!')
    @api.expect(model_hello, validate=True)
    def post(self):
        '''Hello <who>'''
        data = request.json
        
        return "Hello, %s" % data["who"], 200

@api.route('/<string:who>')
class HelloWho(Resource):
    def get(self, who):
        '''Hello who ?'''
        return "Hello, %s" % who, 200

@api.route('/world')
class HelloWorld(Resource):
    def get(self):
        '''HelloWorld'''
        return "Hello, World", 200
    

