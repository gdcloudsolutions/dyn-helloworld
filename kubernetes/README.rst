Loyalty service deployment
==========================

This chart set a deployment for Loyalty service with istio gateway and rbac on. (No ingress entry used)

helm
----

.. code:: bash

    ISTIO_FULLACCESS=<PUT YOUR KEY>
    ISTIO_HEALTH=HEALTH
    helm upgrade --install "loyalty" --namespace "loyalty" ./loyalty/ --set istio.apikey.fullaccess=$ISTIO_FULLACCESS --set istio.apikey.healthcheck=$ISTIO_HEALTH

    
test
----

.. code:: bash

    export INGRESS_HOST=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
    export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].port}')
    curl http://$INGRESS_HOST:$INGRESS_PORT/health -H 'Host: loyalty.service.dynamite.ca' -H "apikey:${ISTIO_FULLACCESS}"
    
