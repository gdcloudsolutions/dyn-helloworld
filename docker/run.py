from helloworld.config import Config
from helloworld.app import App

secrets = Config.load_json("secret.json")

config = Config()

ms = App(config)

if __name__ == "__main__":
    ms.run()
else:
    # set application variable for wsgi use
    application = ms.app
    
