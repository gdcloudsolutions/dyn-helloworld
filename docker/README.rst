Docker
------

Create an image
^^^^^^^^^^^^^^^

.. code:: bash
    
    export VERSION=1
    docker build -t dyn-helloworld:$VERSION -f docker/Dockerfile .

secret.json
^^^^^^^^^^^

Check the README.rst on the root of the project.

