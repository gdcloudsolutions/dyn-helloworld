Dynamite helloworld Service
===========================

Docker
------

The docker folder provides everything to create an image of this service.

helloworld module
-----------------

Installation
^^^^^^^^^^^^

This package is available for Python 3.5+.

Install the development version from repo:

.. code:: bash

    pip3 install .

Prerequisite
^^^^^^^^^^^^

Examples in this README are using the secret.json file to inject secrets in the service.
Of course you can use any other solution provided by your infrastructure.

.. code:: python
    
    # Secrets structure
    #
    secrets = {
    }

Quick start
^^^^^^^^^^^

.. code:: bash

    python3 ./run.py
